<?php
namespace iwaystatistics\crap;
use iwaystatistics\base\AbstractProcessor;
require_once (__DIR__.'/../base/AbstractProcessor.php');
require_once (__DIR__.'/Parser.php');
require_once (__DIR__.'/DatabasePersister.php');

class Processor extends AbstractProcessor {
	public function execute($logFilename) {
		$parser = new \iwaystatistics\crap\Parser ();
		$dataLog = $parser->parse ( $logFilename );
		
		$data = array (
				'executionId' => $this->executionId,
				'data' => $dataLog 
		);
		
		$databasePersister = new \iwaystatistics\crap\DatabasePersister ();
		$databasePersister->setDb ( $this->db );
		$databasePersister->persist ( $data );
	}
}
