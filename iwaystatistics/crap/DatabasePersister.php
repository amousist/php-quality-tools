<?php
namespace iwaystatistics\crap;
use iwaystatistics\base\AbstractDatabasePersister;
require_once (__DIR__.'/../base/AbstractDatabasePersister.php');

class DatabasePersister extends AbstractDatabasePersister{
	public function persist(array $data) {
		extract ( $data );
		
		$sql = <<<EOQ
INSERT INTO crapmetrics (execution_id, 
						 method_count, 
						 crap_method_count, 
						 crap_load, 
						 total_crap,
						 crap_method_percent)
VALUES (?, ?, ?, ?, ?, ?)
EOQ;
		$stmt = $this->db->prepare ( $sql );
		
		$stmt->bind_param ( "iiiiid", 
							$executionId,
							$data ['method_count'],
							$data ['crap_method_count'],
							$data ['crap_load'],
							$data ['total_crap'],
							$data ['crap_method_percent']);
		$stmt->execute ();
		$stmt->close ();
	}
}
