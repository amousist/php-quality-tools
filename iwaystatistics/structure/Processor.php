<?php

namespace iwaystatistics\structure;
use iwaystatistics\base\AbstractProcessor;
require_once (__DIR__.'/../base/AbstractProcessor.php');
require_once (__DIR__.'/Parser.php');
require_once (__DIR__.'/DatabasePersister.php');

class Processor extends AbstractProcessor {
	public function execute($logFilename) {
		$parser = new \iwaystatistics\structure\Parser ();
		$dataLog = $parser->parse ( $logFilename );
		
		$data = array (
				'executionId' => $this->executionId,
				'data' => $dataLog 
		);
		
		$databasePersister = new \iwaystatistics\structure\DatabasePersister ();
		$databasePersister->setDb ( $this->db );
		$databasePersister->persist ( $data );
	}
}
