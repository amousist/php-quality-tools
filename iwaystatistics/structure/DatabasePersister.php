<?php

namespace iwaystatistics\structure;

use iwaystatistics\base\AbstractDatabasePersister;

require_once (__DIR__ . '/../base/AbstractDatabasePersister.php');
class DatabasePersister extends AbstractDatabasePersister {
	public function persist(array $data) {
		extract ( $data );
		
		$sql = <<<EOQ
INSERT INTO structuremetrics (execution_id, 
							  directories,
							  files,
							  loc,
							  cyc_complex_per_loc,
							  cloc, ncloc, lloc,
							  lloc_outside,
							  namespaces,
							  interfaces,
							  traits,
							  classes,
							  abstract_classes,
							  concrete_classes,
							  classes_length,
							  average_class_length,
							  methods, non_static_methods,
							  static_methods,
							  public_methods,
							  non_public_methods,
							  average_method_length,
							  cyc_complex_per_methods,
							  functions, named_functions,
							  anonym_functions,
							  average_functions_length,
							  constants, global_constants,
							  class_constants,
							  attr_accesses,
							  non_static_attr_accesses,
							  static_method_accesses,
							  global_accesses,
							  global_variable_accesses,
							  su_global_variable_accesses,
							  global_constant_accesses,
							  test_classes,
							  test_methods)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
EOQ;
		$stmt = $this->db->prepare ( $sql );
		$stmt->bind_param ( "iiiidiiiiiiiiiiidiiiiiddiiidiiiiiiiiiiii", 
							$executionId,
							$data ['directories'],
							$data ['files'],
							$data ['loc'],
							$data ['cyc_complex_per_loc'],
							$data ['cloc'],
							$data ['ncloc'],
							$data ['lloc'],
							$data ['lloc_outside'],
							$data ['namespaces'], 
							$data ['interfaces'], 
							$data ['traits'], 
							$data ['classes'], 
							$data ['abstract_classes'], 
							$data ['concrete_classes'], 
							$data ['classes_length'], 
							$data ['average_class_length'], 
							$data ['methods'], 
							$data ['non_static_methods'], 
							$data ['static_methods'], 
							$data ['public_methods'], 
							$data ['non_public_methods'], 
							$data ['average_method_length'], 
							$data ['cyc_complex_per_methods'], 
							$data ['functions'], 
							$data ['named_functions'], 
							$data ['anonym_functions'],
							$data ['average_functions_length'],
							$data ['constants'],
							$data ['global_constants'],
							$data ['class_constants'],
							$data ['attr_accesses'],
							$data ['non_static_attr_accesses'],
							$data ['static_method_accesses'],
							$data ['global_accesses'],
							$data ['global_variable_accesses'],
							$data ['su_global_variable_accesses'],
							$data ['global_constant_accesses'],
							$data ['test_classes'],
							$data ['test_methods'] );
		$stmt->execute ();
		$stmt->close ();
	}
}
