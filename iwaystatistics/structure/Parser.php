<?php

namespace iwaystatistics\structure;

class Parser {
	public function parse($filename) {
		$handle = fopen ( $filename, "r" );
		if ($handle) {
			// Cabecera...
			$csvRow = fgetcsv ( $handle, 4096, "," );
			
			// Datos
			$csvRow = fgetcsv ( $handle, 4096, "," );
			$data = array (
					'directories' => ( int ) trim ( $csvRow [0] ),
					'files' => ( int ) trim ( $csvRow [1] ),
					'loc' => ( int ) trim ( $csvRow [2] ),
					'cyc_complex_per_loc' => trim ( $csvRow [3] ),
					'cloc' => ( int ) trim ( $csvRow [4] ),
					'ncloc' => ( int ) trim ( $csvRow [5] ),
					'lloc' => ( int ) trim ( $csvRow [6] ),
					'lloc_outside' => ( int ) trim ( $csvRow [7] ),
					'namespaces' => ( int ) trim ( $csvRow [8] ),
					'interfaces' => ( int ) trim ( $csvRow [9] ),
					'traits' => ( int ) trim ( $csvRow [10] ),
					'classes' => ( int ) trim ( $csvRow [11] ),
					'abstract_classes' => ( int ) trim ( $csvRow [12] ),
					'concrete_classes' => ( int ) trim ( $csvRow [13] ),
					'classes_length' => ( int ) trim ( $csvRow [14] ),
					'average_class_length' => trim ( $csvRow [15] ),
					'methods' => ( int ) trim ( $csvRow [16] ),
					'non_static_methods' => ( int ) trim ( $csvRow [17] ),
					'static_methods' => ( int ) trim ( $csvRow [18] ),
					'public_methods' => ( int ) trim ( $csvRow [19] ),
					'non_public_methods' => ( int ) trim ( $csvRow [20] ),
					'average_method_length' => trim ( $csvRow [21] ),
					'cyc_complex_per_methods' => trim ( $csvRow [22] ),
					'functions' => ( int ) trim ( $csvRow [23] ),
					'named_functions' => ( int ) trim ( $csvRow [24] ),
					'anonym_functions' => ( int ) trim ( $csvRow [25] ),
					'average_functions_length' => trim ( $csvRow [26] ),
					'constants' => ( int ) trim ( $csvRow [27] ),
					'global_constants' => ( int ) trim ( $csvRow [28] ),
					'class_constants' => ( int ) trim ( $csvRow [29] ),
					'attr_accesses' => ( int ) trim ( $csvRow [30] ),
					'non_static_attr_accesses' => ( int ) trim ( $csvRow [31] ),
					'static_method_accesses' => ( int ) trim ( $csvRow [32] ),
					'global_accesses' => ( int ) trim ( $csvRow [33] ),
					'global_variable_accesses' => ( int ) trim ( $csvRow [34] ),
					'su_global_variable_accesses' => ( int ) trim ( $csvRow [35] ),
					'global_constant_accesses' => ( int ) trim ( $csvRow [36] ),
					'test_classes' => ( int ) trim ( $csvRow [37] ),
					'test_methods' => ( int ) trim ( $csvRow [38] ) 
			);
		} else {
			return false;
		}
		fclose ( $handle );
		unset ( $handle );
		return $data;
	}
}
