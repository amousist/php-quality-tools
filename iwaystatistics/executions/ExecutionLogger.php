<?php

namespace iwaystatistics\executions;

class ExecutionLogger {
	public function execute(\Mysqli $mysqli, $project, $businessUnit, $product, $release, $buildUser, $language) {
		$date = date ( 'Y-m-d H:i:s' );
		
		$sql = <<<EOQ
SELECT id FROM projects where project_name = ? AND bu = ? AND release_name = ? AND language = ?
EOQ;
		$stmt = $mysqli->prepare ( $sql );
		$stmt->bind_param ( "sssss", $project, $businessUnit, $release, $language);
		$stmt->execute ();
		$stmt->bind_result ( $projectId );
		$stmt->fetch ();
		$stmt->close ();
		
		if (! isset ( $projectId )) {
			// Insert
			$sql = <<<EOQ
INSERT INTO projects (project_name, bu, product, release_name, language) VALUES (?, ?, ?, ?, ?)
EOQ;
			
			$stmt = $mysqli->prepare ( $sql );
			$stmt->bind_param ( "sssss", $project, $businessUnit, $product, $release, $language);
			$stmt->execute ();
			$projectId = $mysqli->insert_id;
			$stmt->close ();
		}
		
		$sql = <<<EOQ
INSERT INTO executions (project_id, fecha, build_user) VALUES (?, ?, ?)
EOQ;
		
		$stmt = $mysqli->prepare ( $sql );
		$stmt->bind_param ( "iss", $projectId, $date, $buildUser );
		$stmt->execute ();
		
		$executionId = $mysqli->insert_id;
		$stmt->close ();
		
		return $executionId;
	}
}
