<?php

namespace iwaystatistics\mess;

class Parser {
	public function parse($filename) {
		$messes = array (
				'1' => array(
						'amount' => 0
				),
				'2' => array(
						'amount' => 0
				),
				'3' => array(
						'amount' => 0
				),
				'4' => array(
						'amount' => 0
				),
				'5' => array(
						'amount' => 0
				)
		);
		
		if (file_exists ( $filename )) {
			
			$oXMLParser = simplexml_load_file ( $filename );
			foreach ( $oXMLParser -> file  as $file ) {
				foreach ($file -> violation as $mess){
					$messes[(int) $mess['priority']]['amount'] ++;
				}
			}
		} 
		return $messes;
	}
}
