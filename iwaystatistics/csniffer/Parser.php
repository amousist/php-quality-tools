<?php

namespace iwaystatistics\csniffer;

class Parser {
	protected $totales = array (
			'errors' => 0,
			'warnings' => 0 
	);
	public function parse($filename) {
		$matches = array ();
		$subject = file_get_contents ( $filename );
		$pattern = '/A TOTAL OF ([0-9]+) ERROR\(S\) AND ([0-9]+) WARNING\(S\)/';
		$r = preg_match ( $pattern, $subject, $matches );
		if ($r === 1) {
			$this->totales ['errors'] = ( int ) isset ( $matches [1] ) ? $matches [1] : 0;
			$this->totales ['warnings'] = ( int ) isset ( $matches [2] ) ? $matches [2] : 0;
		} else {
			$this->totales ['errors'] = 0;
			$this->totales ['warnings'] = 0;
		}
		
		return $this->totales;
	}
}
