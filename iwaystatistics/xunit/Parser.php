<?php

namespace iwaystatistics\xunit;

class Parser {
	public function parseLog($filename) {
		if (file_exists ( $filename )) {
			
			$oXMLParser = simplexml_load_file ( $filename );
			$aAttr = $oXMLParser->testsuite->attributes ();
			$data = array (
					'total_tests' => ( int ) $aAttr->tests,
					'total_ok' => ( int ) $aAttr->assertions,
					'total_nok' => ( int ) $aAttr->failures,
					'total_errors' => ( int ) $aAttr->errors 
			);
		} else {
			$data = array (
					'total_tests' => 0,
					'total_ok' => 0,
					'total_nok' => 0,
					'total_errors' => 0 
			);
		}
		unset ( $aAttr, $oXMLParser );
		return $data;
	}
	
	
	public function parseCoverage($filename) {
		if (file_exists ( $filename )) {
			$oXMLParser = simplexml_load_file ( $filename );
			$aAttr = $oXMLParser->project->metrics->attributes ();
			$statements = ( int ) $aAttr->statements;
			
			if ($statements > 0) {
				$porcCoveredStatements = round ( ((( int ) $aAttr->coveredstatements * 100) / ( int ) $aAttr->statements), 2 );
			} else {
				$porcCoveredStatements = 0;
			}
			
			$data = array (
					'total_statements' => ( int ) $aAttr->statements,
					'coverage_statements' => ( int ) $aAttr->coveredstatements,
					'porcentual_covered_statements' => $porcCoveredStatements 
			);
		} else {
			$data = array (
					'total_statements' => 0,
					'coverage_statements' => 0,
					'porcentual_covered_statements' => 0 
			);
		}
		unset ( $aAttr, $oXMLParser );
		return $data;
	}
}
