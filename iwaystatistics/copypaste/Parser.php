<?php

namespace iwaystatistics\copypaste;

class Parser {
	public function parse($filename) {
				
		if (file_exists ( $filename )) {
			
			$oXMLParser = simplexml_load_file ( $filename );
			$data = array (
					'duplications' => ( int ) $oXMLParser->count()
			);
		} 
		else{
			$data = array (
					'duplications' => 0
			);
		}
		return $data;
	}
}
