<?php

namespace iwaystatistics\copypaste;

use iwaystatistics\base\AbstractDatabasePersister;

require_once (__DIR__ . '/../base/AbstractDatabasePersister.php');
class DatabasePersister extends AbstractDatabasePersister {
	public function persist(array $data) {
		extract ( $data );
		if ($data ['duplications'] != 0) {
			$sql = <<<EOQ
INSERT INTO duplicationmetrics (execution_id, duplications)
VALUES (?, ?)
EOQ;
			$stmt = $this->db->prepare ( $sql );
			$stmt->bind_param ( "ii", $executionId, $data ['duplications'] );
			$stmt->execute ();
			$stmt->close ();
		}
	}
}
