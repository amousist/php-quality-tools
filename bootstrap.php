<?php
/**
 * Test bootstrap
 *
 * PHP version 5.3+
 *
 * @category  Tests
 * @package   PlatAppsSkeleton/Component/Tests
 * @author    Federico Lozada Mosto <federico.mosto@intraway.com>
 * @copyright 2014 Intraway Corp.
 * @license   http://www.intraway.com Intraway
 * @link      http://www.intraway.com
 */
$path = dirname(__FILE__);
require_once $path.'/vendor/autoload.php';
