<?php
require_once (__DIR__ . '/iwaystatistics/csniffer/Processor.php');
require_once (__DIR__ . '/iwaystatistics/phpcompat/Processor.php');
require_once (__DIR__ . '/iwaystatistics/xunit/Processor.php');
require_once (__DIR__ . '/iwaystatistics/crap/Processor.php');
require_once (__DIR__ . '/iwaystatistics/structure/Processor.php');
require_once (__DIR__ . '/iwaystatistics/mess/Processor.php');
require_once (__DIR__ . '/iwaystatistics/copypaste/Processor.php');
require_once (__DIR__ . '/iwaystatistics/executions/ExecutionLogger.php');

// obtiene los parametros de consola
$project = isset ( $_SERVER ['argv'] [1] ) ? $_SERVER ['argv'] [1] : null;
$release = isset ( $_SERVER ['argv'] [2] ) ? $_SERVER ['argv'] [2] : null;
$businessUnit = isset ( $_SERVER ['argv'] [3] ) ? $_SERVER ['argv'] [3] : null;
$product = isset ( $_SERVER ['argv'] [4] ) ? $_SERVER ['argv'] [4] : null;
$buildUser = isset ( $_SERVER ['argv'] [5] ) ? $_SERVER ['argv'] [5] : null;
$xuFilelog = isset ( $_SERVER ['argv'] [6] ) ? $_SERVER ['argv'] [6] : null;
$xuFileCov = isset ( $_SERVER ['argv'] [7] ) ? $_SERVER ['argv'] [7] : null;
$csFilelog = isset ( $_SERVER ['argv'] [8] ) ? $_SERVER ['argv'] [8] : null;
$pvFilelog = isset ( $_SERVER ['argv'] [9] ) ? $_SERVER ['argv'] [9] : null;
$cjFilelog = isset ( $_SERVER ['argv'] [10] ) ? $_SERVER ['argv'] [10] : null;
$stFilelog = isset ( $_SERVER ['argv'] [11] ) ? $_SERVER ['argv'] [11] : null;
$mdFilelog = isset ( $_SERVER ['argv'] [12] ) ? $_SERVER ['argv'] [12] : null;
$cdFilelog = isset ( $_SERVER ['argv'] [13] ) ? $_SERVER ['argv'] [13] : null;


$dbArray = parse_ini_file ( "db.ini" );
$mysqli = new \Mysqli ( $dbArray ["dbIp"], $dbArray ["dbUser"], $dbArray ["dbPass"], $dbArray ["dbSchema"], $dbArray ["dbPort"] );

/**
 * Execution & project
 */
$executionLogger = new iwaystatistics\executions\ExecutionLogger();
$executionId = $executionLogger->execute($mysqli, $project, $businessUnit, $product, $release, $buildUser, "PHP");


/**
 * PHPUnit
 */
if (! isset($xuFilelog) || ! isset($xuFileCov) ) {
	echo "No unit test (PHPUnit) metrics to save to database" . PHP_EOL;
}
else{
	$xuProcessor = new iwaystatistics\xunit\Processor ( $mysqli, $executionId );
	$xuProcessor->execute ( $xuFilelog, $xuFileCov );
}


/**
 * PHPCodeSniffer (code standard)
 */
if (!isset($csFilelog)) {
	echo "No coding standards (PHPCodeSniffer) metrics to save to database" . PHP_EOL;
}
else{
	$csProcessor = new iwaystatistics\csniffer\Processor ( $mysqli, $executionId );
	$csProcessor->execute ( $csFilelog );
}


/**
 * PHPCodeSniffer (Php compatibility)
 */
if (!isset($pvFilelog)) {
	echo "No php compatibility (PHPCodeSniffer) metrics to save to database" . PHP_EOL;
}
else{
	$pvProcessor = new iwaystatistics\phpcompat\Processor ( $mysqli, $executionId );
	$pvProcessor->execute ( $pvFilelog );
}


/**
 *  Crap4J
 */
if (!isset($cjFilelog)) {
	echo "No crappinnes (Crap4J) code metrics to save to database" . PHP_EOL;
}
else{
	$cjProcessor = new iwaystatistics\crap\Processor ( $mysqli, $executionId );
	$cjProcessor->execute ( $cjFilelog );
}


/**
 *  PHPLoc
 */
if (!isset($stFilelog)) {
	echo "No structure(PHPLoc) metrics to save to database" . PHP_EOL;
}
else{
	$stProcessor = new iwaystatistics\structure\Processor ( $mysqli, $executionId );
	$stProcessor->execute ( $stFilelog );
}


/**
 * PHPMD
 */
if (!isset($mdFilelog)) {
	echo "No mess (PHPMD) metrics to save to database" . PHP_EOL;
}
else{
	$mdProcessor = new iwaystatistics\mess\Processor ( $mysqli, $executionId );
	$mdProcessor->execute ( $mdFilelog );
}


/**
 * PHP-CPD
 */
if (!isset($cdFilelog)) {
	echo "No copy/paste (CPD) metrics to save to database" . PHP_EOL;
}
else{
	$cdProcessor = new iwaystatistics\copypaste\Processor ( $mysqli, $executionId );
	$cdProcessor->execute ( $cdFilelog );
}

echo "END" . PHP_EOL;
